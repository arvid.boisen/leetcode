from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        result = ""
        count = 0
        while True:
            if len(strs[0]) <= count:
                return result
            character = strs[0][count]
            for word in strs[1:]:
                if len(word) <= count:
                    return result
                if word[count] != character:
                    return result
            result += character
            count += 1
