from math import floor, log

class Solution:
    def countSymmetricIntegers(self, low: int, high: int) -> int:
        count=0
        for num in range(low,high+1):
            length = floor(log(num, 10) + 1)
            if length & 1 == 1:
                continue
            n = length//2
            string = str(num)
            if  sum(int(x) for x in string[:n] if '0' <= x <= '9')  == sum(int(x) for x in string[n:] if '0' <= x <= '9'):
                count+=1
        return count