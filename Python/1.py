from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        hashset = {}
        for i, num in enumerate(nums):
            complement = target - num
            if complement in hashset:
                res = hashset[complement]
                return [res, i]
            hashset[num]= i
        return []
