from math import floor, log


class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        if x == 0:
            return True
        length = floor(log(x, 10) + 1)
        result = 0
        k = 0
        while k < length // 2:
            result *= 10
            result += x % 10
            x //= 10
            k += 1
        if length & 1 == 1:
            x //= 10

        return x == result
