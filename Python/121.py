from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        maxProfit=0
        min_value = 10**4
        for price in prices:
            if price < min_value:
                min_value = price
            else:
                maxProfit = max(maxProfit,price-min_value)

        return maxProfit