use std::collections::HashSet;

pub struct Solution {}

impl Solution {
    pub fn find_duplicate(nums: Vec<i32>) -> i32 {
        let xor = nums.iter().fold(0, |acc, x| acc ^ x);
        println!("{}",xor);
        let mut hashset = HashSet::new();

        for num in nums {
            if !hashset.insert(num) {
                return num;
            }
        }
        return -1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::find_duplicate(vec![1, 3, 4, 2, 2]), 2);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::find_duplicate(vec![3, 1, 3, 4, 2]), 3);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::find_duplicate(vec![3, 3, 3, 3, 3]), 3);
    }
    #[test]
    fn test_4() {
        assert_eq!(Solution::find_duplicate(vec![0,0]), 0);
    }
}

fn main() {}
