pub struct Solution {}

impl Solution {
    pub fn roman_to_int(s: String) -> i32 {
        //create dictionary to store roman values
        use std::collections::HashMap;
        let roman_lib: HashMap<char, i32> = HashMap::from([
            ('I', 1),
            ('V', 5),
            ('X', 10),
            ('L', 50),
            ('C', 100),
            ('D', 500),
            ('M', 1000),
        ]);

        let mut res = 0;
        let mut prev = 0;
        for char in s.to_uppercase().chars().rev() {
            if let Some(num) = roman_lib.get(&char) {
                if *num < prev {
                    res -= num;
                } else {
                    res += num;
                }
                prev = *num;
            } else {
                panic!("The character {} could not be recognised", char);
            }
        }
        return res;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(3, Solution::roman_to_int(String::from("III")));
    }
    #[test]
    fn test_2() {
        assert_eq!(58, Solution::roman_to_int(String::from("LVIII")));
    }
    #[test]
    fn test_3() {
        assert_eq!(1994, Solution::roman_to_int(String::from("MCMXCIV")));
    }
}

fn main() {}
