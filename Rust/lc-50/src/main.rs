pub struct Solution {}

impl Solution {
    pub fn recursive_pow(accumulator: f64, factor: f64, n: i64) -> f64 {
        if n == 0 {
            accumulator
        } else {
            Self::recursive_pow(
                if n & 1 == 1 { accumulator * factor } else { accumulator },
                factor * factor,
                n >> 1,
            )
        }
    }

    pub fn my_pow(x: f64, n: i32) -> f64 {
        format!("{:.5}",Self::recursive_pow(
            1.0,
             if n >= 0 {
                x
            } else {
                1.0 / x
            },
            if n >= 0 { n as i64 } else { -(n as i64) },
        )).parse().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::my_pow(2.0, 10), 1024.0);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::my_pow(2.10, 3), 9.26100);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::my_pow(2.0, -2), 0.2500);
    }
    #[test]
    fn test_4() {
        assert_eq!(Solution::my_pow(0.44894, -5), 54.83508);
    }
}

fn main() {}
