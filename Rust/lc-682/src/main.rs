pub struct Solution {}

impl Solution {
    pub fn cal_points(operations: Vec<String>) -> i32 {
        let mut stack: Vec<i32> = Vec::new();

        for op in operations {
            match op.as_str() {
                "+" => {
                    let last = stack[stack.len() - 1];
                    let second_last = stack[stack.len() - 2];
                    stack.push(last + second_last);
                }
                "D" => {
                    let last = stack[stack.len() - 1];
                    stack.push(2 * last);
                }
                "C" => {
                    stack.pop();
                }
                _ => {
                    let num = op.parse().unwrap();
                    stack.push(num);
                }
            }
        }
        stack.iter().sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::cal_points(vec![
                "5".to_owned(),
                "2".to_owned(),
                "C".to_owned(),
                "D".to_owned(),
                "+".to_owned()
            ]),
            30
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            Solution::cal_points(vec![
                "5".to_owned(),
                "-2".to_owned(),
                "4".to_owned(),
                "C".to_owned(),
                "D".to_owned(),
                "9".to_owned(),
                "+".to_owned(),
                "+".to_owned()
            ]),
            27
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            Solution::cal_points(vec!["1".to_owned(), "C".to_owned()]),
            0
        );
    }
}

fn main() {}
