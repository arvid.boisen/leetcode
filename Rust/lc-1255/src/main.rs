use std::collections::{BinaryHeap, HashMap};

pub struct Solution {}

impl Solution {
    pub fn max_score_words(words: Vec<String>, letters: Vec<char>, score: Vec<i32>) -> i32 {
        fn back_track(words: &[String], count: &mut [i32], score: &[i32], index: usize) -> i32 {
            let mut max = 0;
            for i in index..words.len() {
                let mut res = 0;
                let mut is_valid = true;
                for &ch in words[i].as_bytes() {
                    count[(ch as u8 - b'a') as usize] -= 1;
                    res += score[(ch as u8 - b'a') as usize];
                    if count[(ch as u8 - b'a') as usize] < 0 {
                        is_valid = false;
                    }
                }
                if is_valid {
                    res += back_track(words, count, score, i + 1);
                    if res > max {
                        max = res;
                    }
                }
                for &ch in words[i].as_bytes() {
                    count[(ch as u8 - b'a') as usize] += 1;
                }
            }
            max
        }
        let mut count = vec![0; 26];
        for &ch in &letters {
            count[(ch as u8 - b'a') as usize] += 1;
        }
        back_track(&words, &mut count, &score, 0)
    }

    pub fn max_score_words1(words: Vec<String>, letters: Vec<char>, score: Vec<i32>) -> i32 {
        fn back() -> i32 {
            5
        }
        back();
        let mut map = HashMap::new();
        for (i, char) in score.iter().enumerate() {
            map.insert(('a' as u8 + i as u8) as char, char);
        }
        let mut words_score = Vec::new();
        let mut heap = BinaryHeap::new();
        'outer: for word in words {
            let mut score = 0;
            let mut letters1 = letters.clone();
            for char in word.chars() {
                if let Some(pos) = letters1.iter().position(|f| f == &char) {
                    // let char_vec = word.chars().collect();
                    // if let Some(char_at) = word.chars().nth(pos){

                    // if letters1.contains(&char_at) {
                    score += *map.get(&char).unwrap();
                    letters1.remove(pos);
                    // word.get(pos..=pos)

                    // }
                } else {
                    heap.push(0);
                    words_score.push(0);
                    continue 'outer;
                }
            }
            heap.push(score);
            words_score.push(score);
        }
        if heap.len() > 1 {
            heap.pop().unwrap() + heap.pop().unwrap()
        } else {
            heap.pop().unwrap()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            23,
            Solution::max_score_words(
                vec![
                    "dog".to_string(),
                    "cat".to_string(),
                    "dad".to_string(),
                    "good".to_string()
                ],
                vec!['a', 'a', 'c', 'd', 'd', 'd', 'g', 'o', 'o'],
                vec![1, 0, 9, 5, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            )
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            27,
            Solution::max_score_words(
                vec![
                    "xxxz".to_string(),
                    "ax".to_string(),
                    "bx".to_string(),
                    "cx".to_string()
                ],
                vec!['z', 'a', 'b', 'c', 'x', 'x', 'x'],
                vec![4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 10]
            )
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            0,
            Solution::max_score_words(
                vec!["leetcode".to_string()],
                vec!['l', 'e', 't', 'c', 'o', 'd'],
                vec![0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
            )
        );
    }
}

fn main() {}
