pub struct Solution {}

impl Solution {
    pub fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, n: i32) {
        if m == 0 {
            while !nums1.is_empty() {
                nums1.pop();
            }
        }
        for _ in 0..m {
            if n != 0 {
                nums1.pop();
            }
        }
        for num in nums2 {
            nums1.push(*num);
        }
        nums1.sort()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let mut nums1 = vec![1,2,3,0,0,0];
        let mut nums2 = vec![2,5,6];
        Solution::merge(&mut nums1, 3, &mut nums2, 3);
        assert_eq!(
            nums1,
            vec![1,2,2,3,5,6]
        );
    }
    #[test]
    fn test_2() {
        let mut nums1 = vec![1];
        let mut nums2 = vec![];
        Solution::merge(&mut nums1, 1, &mut nums2, 0);
        assert_eq!(
            nums1,
            vec![1]
        );
    }
    #[test]
    fn test_3() {
        let mut nums1 = vec![0];
        let mut nums2 = vec![1];
        Solution::merge(&mut nums1, 0, &mut nums2, 1);
        assert_eq!(
            nums1,
            vec![1]
        );
    }
    // #[test]
    // fn test_2() {
    //     assert_eq!(false, Solution::is_palindrome(String::from("race a car")));
    // }
    // #[test]
    // fn test_3() {
    //     assert_eq!(true, Solution::is_palindrome(String::from(" ")));
    // }
}

fn main() {
    // Solution::is_palindrome(String::from("A man, a plan, a canal: Panama"));
}
