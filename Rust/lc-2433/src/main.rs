pub struct Solution {}

impl Solution {
    pub fn find_array(pref: Vec<i32>) -> Vec<i32> {
        // let mut arr: Vec<i32> = Vec::with_capacity(pref.len());
        // let mut sum=0;
        // for num in pref {
        //     let res = sum^num;
        //     arr.push(res);
        //     sum^=res;
        // }
        // arr
        let mut result = Vec::with_capacity(pref.len());
        result.push(pref[0]);
        for i in 1..pref.len() {
            result.push(pref[i] ^ pref[i-1]);
        }
        result
    }
}

// x ^ y = z <==> x ^ z = y 

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            vec![5, 7, 2, 3, 2],
            Solution::find_array(vec![5, 2, 0, 3, 1])
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(vec![13], Solution::find_array(vec![13]));
    }
}

fn main() {}
