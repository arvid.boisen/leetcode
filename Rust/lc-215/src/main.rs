pub struct Solution {}

impl Solution {
    // Idea split spring in the middle if odd then we dont care about middle
    pub fn is_palindrome(mut s: String) -> bool {
        s.retain(|f| f.is_alphanumeric());
        let s = s.to_lowercase();
        let len = s.len() / 2;
        let extra = s.len() & 1;
        s[..len].chars().eq(s[len + extra..].chars().rev())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::is_palindrome(String::from("A man, a plan, a canal: Panama")),
            true
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::is_palindrome(String::from("race a car")), false);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::is_palindrome(String::from(" ")), true);
    }
}

fn main() {}
