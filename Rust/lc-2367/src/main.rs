use std::collections::HashSet;

pub struct Solution {}

impl Solution {
    pub fn arithmetic_triplets(nums: Vec<i32>, diff: i32) -> i32 {
        if nums.len() < 3 {
            return 0;
        }
        let mut count = 0;

        let mut set = HashSet::new();

        for num in nums {
            if set.contains(&(num - diff)) && set.contains(&(num - diff * 2)) {
                count += 1;
            }
            set.insert(num);
        }

        count
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(2, Solution::arithmetic_triplets(vec![0, 1, 4, 6, 7, 10], 3));
    }
    #[test]
    fn test_2() {
        assert_eq!(2, Solution::arithmetic_triplets(vec![4, 5, 6, 7, 8, 9], 2));
    }
}

fn main() {}
