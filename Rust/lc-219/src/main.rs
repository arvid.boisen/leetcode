use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn contains_nearby_duplicate(nums: Vec<i32>, k: i32) -> bool {
        let mut hashmap: HashMap<i32, usize> = HashMap::new();
        for (i, n) in nums.into_iter().enumerate() {
            let j = hashmap.entry(n).or_insert(i);
            if *j != i && i - *j <= k as usize {
                return true;
            } else {
                *j = i;
            }
        }
        // for (i, num) in nums.iter().enumerate() {
        //     if let Some(val) = hashmap.get(num) {
        //         let diff = i - val;
        //         if diff <= k as usize{
        //             return true;
        //         }
        //     }
        //     hashmap.insert(*num, i);
        // }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            true,
            Solution::contains_nearby_duplicate(vec![1, 2, 3, 1], 3)
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            true,
            Solution::contains_nearby_duplicate(vec![1, 0, 1, 1], 1)
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            false,
            Solution::contains_nearby_duplicate(vec![1, 2, 3, 1, 2, 3], 2)
        );
    }
}

fn main() {}
