pub struct Solution {}

impl Solution {
    pub fn missing_number(nums: Vec<i32>) -> i32 {
        let n = nums.len() as i32;
        let sum = n * (n + 1) / 2;
        let sum_arr: i32 = nums.iter().sum();
        sum - sum_arr
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::missing_number(vec![3, 0, 1]), 2);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::missing_number(vec![0, 1]), 2);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::missing_number(vec![9, 6, 4, 2, 3, 5, 7, 0, 1]), 8);
    }
}

fn main() {}
