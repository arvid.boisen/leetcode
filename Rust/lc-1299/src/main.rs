pub struct Solution {}

impl Solution {
    pub fn replace_elements(mut arr: Vec<i32>) -> Vec<i32> {
        let mut max_value = -1;
    
        for i in (0..arr.len()).rev() {
            let current = arr[i];
            arr[i] = max_value;
            max_value = max_value.max(current);
        }
        
        arr
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            vec![18, 6, 6, 6, 1, -1],
            Solution::replace_elements(vec![17, 18, 5, 4, 6, 1])
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(vec![-1], Solution::replace_elements(vec![400]));
    }
}

fn main() {}
