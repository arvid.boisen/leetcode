pub struct Solution {}

impl Solution {
    pub fn plus_one(mut digits: Vec<i32>) -> Vec<i32> {
        let mut len = digits.len() - 1;

        if 9 != digits[len] {
            digits[len] += 1;
            return digits;
        }

        let mut wrapped = false;
        while 9 == digits[len] {
            digits[len] = 0;

            if len == 0 {
                wrapped = true;
                break;
            }
            len -= 1;
        }
        if wrapped {
            digits.push(1);
            digits.reverse();
        } else {
            digits[len] += 1;
        }
        digits
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(vec![1, 2, 4], Solution::plus_one(vec![1, 2, 3]));
    }
    #[test]
    fn test_2() {
        assert_eq!(vec![4, 3, 2, 2], Solution::plus_one(vec![4, 3, 2, 1]));
    }
    #[test]
    fn test_3() {
        assert_eq!(vec![1, 0], Solution::plus_one(vec![9]));
    }
    #[test]
    fn test_4() {
        assert_eq!(
            vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 1],
            Solution::plus_one(vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
        );
    }
}

fn main() {}
