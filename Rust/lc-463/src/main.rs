pub struct Solution {}

impl Solution {
    pub fn island_perimeter(grid: Vec<Vec<i32>>) -> i32 {
        let mut counter = 0;
        for row in 0..grid.len() {
            for col in 0..grid[row].len() {
                if grid[row][col] == 1 {
                    counter += 4;
                    if row > 0 && grid[row - 1][col] == 1 {
                        counter -= 2;
                    }
                    if col > 0 && grid[row][col - 1] == 1 {
                        counter -= 2;
                    }
                }
            }
        }
        counter
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let result = Solution::island_perimeter(vec![
            vec![0, 1, 0, 0],
            vec![1, 1, 1, 0],
            vec![0, 1, 0, 0],
            vec![1, 1, 0, 0],
        ]);

        assert_eq!(result, 16);
    }
    #[test]
    fn test_2() {
        let result = Solution::island_perimeter(vec![vec![1]]);

        assert_eq!(result, 4);
    }
    #[test]
    fn test_3() {
        let result = Solution::island_perimeter(vec![vec![1, 0]]);

        assert_eq!(result, 4);
    }
}

fn main() {}
