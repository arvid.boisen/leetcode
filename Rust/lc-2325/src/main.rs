use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn decode_message(key: String, message: String) -> String {
        let mut hashmap = HashMap::new();
        let mut index = b'a';
        for k in key.chars() {
            if k.is_ascii_lowercase() && !hashmap.contains_key(&k) {
                let v = index as char;
                hashmap.insert(k, v);
                index += 1;
            }
        }
        let mut ret = String::new();
        for m in message.chars() {
            if let Some(x) = hashmap.get(&m) {
                ret.push(*x);
            } else {
                ret.push(' ');
            }
        }
        ret
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            String::from("this is a secret"),
            Solution::decode_message(
                String::from("the quick brown fox jumps over the lazy dog"),
                String::from("vkbs bs t suepuv")
            )
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            String::from("the five boxing wizards jump quickly"),
            Solution::decode_message(
                String::from("eljuxhpwnyrdgtqkviszcfmabo"),
                String::from("zwx hnfx lqantp mnoeius ycgk vcnjrdb")
            )
        );
    }
}

fn main() {}
