use std::cmp::Ordering;

pub struct Solution {}

impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        let mut low = 0;
        let mut high = (nums.len() - 1) as i32;

        while high >= low {
            let mid = (high + low) / 2;

            match target.cmp(&nums[mid as usize]) {
                Ordering::Greater => {
                    low = mid + 1;
                }
                Ordering::Less => {
                    high = mid - 1;
                }
                Ordering::Equal => {
                    return mid as i32;
                }
            }
        }
        -1
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::search(vec![5], -5), -1);
    }
}

fn main() {}
