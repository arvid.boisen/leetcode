pub struct Solution {}

impl Solution {
    pub fn find_judge(n: i32, trust: Vec<Vec<i32>>) -> i32 {


        5
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let result = Solution::find_judge(2, vec![vec![1, 2]]);

        assert_eq!(result, 2);
    }
    #[test]
    fn test_2() {
        let result = Solution::find_judge(3, vec![vec![1, 3], vec![2, 3]]);

        assert_eq!(result, 3);
    }
    #[test]
    fn test_3() {
        let result = Solution::find_judge(3, vec![vec![1, 3], vec![2, 3], vec![3, 1]]);

        assert_eq!(result, -1);
    }
    #[test]
    fn test_4() {
        let result = Solution::find_judge(1, vec![vec![1]]);

        assert_eq!(result, -1);
    }
}

fn main() {}
