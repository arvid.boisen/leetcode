use std::collections::VecDeque;

pub struct Solution {}

impl Solution {
    pub fn tribonacci(n: i32) -> i32 {
        let n = n as usize;
        // let mut res = vec![0, 1, 1];
        let mut res = VecDeque::with_capacity(4);
        res.push_back(0);
        res.push_back(1);
        res.push_back(1);

        for _ in 3..=n {
            res.push_back(res[0] + res[1] + res[2]);
            res.pop_front();
        }

        res[if n >2 {2} else {n}]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(4, Solution::tribonacci(4));
    }
    #[test]
    fn test_2() {
        assert_eq!(1389537, Solution::tribonacci(25));
    }
    #[test]
    fn test_3() {
        assert_eq!(0, Solution::tribonacci(0));
    }
}

fn main() {
    for i in 0..=30 {
        // println!("i: {}, val: {}",i,Solution::climb_stairs(i));
        println!(" {} => {},", i, Solution::tribonacci(i));
    }
}
