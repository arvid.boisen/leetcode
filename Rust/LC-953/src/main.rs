pub struct Solution {}


        impl Solution {
            pub fn is_alien_sorted(words: Vec<String>, order: String) -> bool {
                let mut dict = [0; 26];
                let order = order.into_bytes();
                for i in 0..order.len() {
                    dict[(order[i] - b'a') as usize] = i;
                }
        
                for i in 1..words.len() {
                    if Self::cmp(&dict, &words[i - 1], &words[i]) == std::cmp::Ordering::Greater {
                        return false;
                    }
                }
                true
            }
        
            fn cmp(dict: &[usize; 26], a: &String, b: &String) -> std::cmp::Ordering {
                let a = a.as_bytes();
                let b = b.as_bytes();
                let mut i = 0;
                let mut j = 0;
                while i < a.len() && j < b.len() {
                    let x = dict[(a[i] - b'a') as usize];
                    let y = dict[(b[j] - b'a') as usize];
                    if x == y {
                        i += 1;
                        j += 1;
                    } else if x > y {
                        return std::cmp::Ordering::Greater;
                    } else {
                        return std::cmp::Ordering::Less;
                    }
                }
                if i < a.len() {
                    return std::cmp::Ordering::Greater;
                }
                if j < b.len() {
                    return std::cmp::Ordering::Less;
                }
                std::cmp::Ordering::Equal
            }
        }


        // for i in 1..words.len() {
        //     if words[i - 1].starts_with(&words[i]) {
        //         return false;
        //     }
        // }

        // for ordering in order.chars() {
        //     for i in 1..words.len() {
        //         match words[i - 1].find(ordering) {
        //             None => {
        //                 match words[i].find(ordering) {
        //                     None => (),
        //                     Some(_) => return false,
        //                 };
        //             }
        //             Some(val) => {
        //                 match words[i].find(ordering) {
        //                     None => return true,
        //                     Some(val2) => {
        //                         if val2 > val {
        //                             return false;
        //                         }
        //                     }
        //                 };
        //             }
        //         };
        //     }
        // }
        // true
        // }
    


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            true,
            Solution::is_alien_sorted(
                ["hello".to_string(), "leetcode".to_string()].to_vec(),
                "hlabcdefgijkmnopqrstuvwxyz".to_string()
            )
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(
            false,
            Solution::is_alien_sorted(
                ["word".to_string(), "world".to_string(), "row".to_string()].to_vec(),
                "worldabcefghijkmnpqstuvxyz".to_string()
            )
        );
    }

    #[test]
    fn test_3() {
        assert_eq!(
            false,
            Solution::is_alien_sorted(
                ["apple".to_string(), "app".to_string()].to_vec(),
                "abcdefghijklmnopqrstuvwxyz".to_string()
            )
        );
    }
    #[test]
    fn test_4() {
        assert_eq!(false,Solution::is_alien_sorted(["my".to_string(),"f".to_string()].to_vec(), "gelyriwxzdupkjctbfnqmsavho".to_string())
        );
    }

}

fn main(){
    println!("{}",b'a');
    println!("{}",Solution::is_alien_sorted(["my".to_string(),"f".to_string()].to_vec(), "gelyriwxzdupkjctbfnqmsavho".to_string()))
}
