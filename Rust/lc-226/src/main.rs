use std::cell::RefCell;
use std::rc::Rc;

pub struct Solution {}
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
impl Solution {
    pub fn invert_tree(root: Option<Rc<RefCell<TreeNode>>>) -> Option<Rc<RefCell<TreeNode>>> {
        let node = root?;
        // if let Some(node) = root {
            let node = node.borrow();
            return Some(Rc::new(RefCell::new(TreeNode {
                val: node.val,
                left: Solution::invert_tree(node.right.clone()),
                right: Solution::invert_tree(node.left.clone()),
            })));
        // }
        // None
        
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_invert_tree() {
        // Test case 1:
        let tree1 = Some(Rc::new(RefCell::new(TreeNode {
            val: 4,
            left: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
            right: Some(Rc::new(RefCell::new(TreeNode::new(7)))),
        })));

        let expected1 = Some(Rc::new(RefCell::new(TreeNode {
            val: 4,
            left: Some(Rc::new(RefCell::new(TreeNode::new(7)))),
            right: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
        })));

        assert_eq!(Solution::invert_tree(tree1), expected1);

        // Test case 2:
        let tree2 = Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: Some(Rc::new(RefCell::new(TreeNode::new(4)))),
                right: Some(Rc::new(RefCell::new(TreeNode::new(5)))),
            }))),
        })));

        let expected2 = Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: Some(Rc::new(RefCell::new(TreeNode::new(5)))),
                right: Some(Rc::new(RefCell::new(TreeNode::new(4)))),
            }))),
            right: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
        })));

        assert_eq!(Solution::invert_tree(tree2), expected2);

        // Test case 3: Empty tree
        let tree3: Option<Rc<RefCell<TreeNode>>> = None;
        assert_eq!(Solution::invert_tree(tree3), None);
    }
}

fn main() {}
