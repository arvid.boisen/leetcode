pub struct Solution {}

impl Solution {
    pub fn is_palindrome(s: String) -> bool {
        let iter = s
            .chars()
            .filter(|c| c.is_ascii_alphanumeric())
            .map(|c| c.to_ascii_lowercase());
        iter.clone().eq(iter.rev())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            true,
            Solution::is_palindrome(String::from("A man, a plan, a canal: Panama"))
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(false, Solution::is_palindrome(String::from("race a car")));
    }
    #[test]
    fn test_3() {
        assert_eq!(true, Solution::is_palindrome(String::from(" ")));
    }
}

fn main() {
    Solution::is_palindrome(String::from("A man, a plan, a canal: Panama"));
}
