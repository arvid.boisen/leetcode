use std::mem::replace;

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

pub struct Solution {}

impl Solution {
    pub fn reverse_list(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {

        let (mut prev, mut curr) = (None, head);
        while let Some(mut node) = curr  {
            curr = node.next;
      
            node.next = prev;
            
            prev = Some(node);
        }
        prev

        
        // let mut cur_node = None;
        // let mut iter = head.as_ref();

        // while iter != None  {
        //     if let Some(ref node) = iter {
        //         let mut new_node = ListNode::new(node.val);

        //         if cur_node != None {
        //             if let Some(cur_box) = cur_node {
        //                 new_node.next = Some(cur_box);
        //             }
        //         }
        //         cur_node = Some(Box::new(new_node));
        //         iter = node.next.as_ref();
        //     }
        // }
        // cur_node


        // let mut res = None;

        // while let Some(ref node) = head {
        //     let mut curr = ListNode::new(node.val);
        //     curr.next = res;
        //     res = Some(Box::new(curr));
        //     head = node.next.as_ref();
        // }
        // res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode {
                    val: 2,
                    next: Some(Box::new(ListNode {
                        val: 3,
                        next: Some(Box::new(ListNode {
                            val: 4,
                            next: Some(Box::new(ListNode { val: 5, next: None })),
                        })),
                    })),
                })),
            })),
            Solution::reverse_list(Some(Box::new(ListNode {
                val: 5,
                next: Some(Box::new(ListNode {
                    val: 4,
                    next: Some(Box::new(ListNode {
                        val: 3,
                        next: Some(Box::new(ListNode {
                            val: 2,
                            next: Some(Box::new(ListNode { val: 1, next: None })),
                        })),
                    })),
                })),
            })))
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode { val: 2, next: None })),
            })),
            Solution::reverse_list(Some(Box::new(ListNode {
                val: 2,
                next: Some(Box::new(ListNode { val: 1, next: None }))
            })))
        );
    }
}

fn main() {}
