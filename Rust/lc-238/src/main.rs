pub struct Solution {}

impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        let mut result = Vec::with_capacity(nums.len());
        let mut prefix = 1;

        for num in nums.iter() {
            result.push(prefix);
            prefix *= num;
        }
        let mut postfix = 1;
        for (i,num) in nums.iter().enumerate().rev(){ 
            result[i] *= postfix;
            postfix *= num;
        }
        result

        //     let mut answer: Vec<i32> = Vec::with_capacity(nums.len());
        //     for i in 0..nums.len() {
        //         answer.push(Solution::product(&nums, i))
        //     }
        //     answer
        // }

        // pub fn product(nums: &Vec<i32>, n: usize) -> i32 {
        //     let res_left: i32 = nums.get(..n).unwrap().iter().product();
        //     let res_right: i32 = nums.get(n + 1..).unwrap().iter().product();

        //     res_left * res_right
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            vec![24, 12, 8, 6],
            Solution::product_except_self(vec![1, 2, 3, 4])
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            vec![0, 0, 9, 0, 0],
            Solution::product_except_self(vec![-1, 1, 0, -3, 3])
        );
    }
}

fn main() {}

/*
For any nums[i], calculate its left product and calculate its right product, without including nums[i].
Then multiply these left and right product, This will give product of array excluding nums[i].
*/

// let mut res = Vec::with_capacity(nums.len());
// let mut prefix = 1;

// for i in 0..nums.len() {
//     res.push(prefix);
//     prefix *= nums[i];
// }

// let mut postfix = 1;
// for i in (0..nums.len()).rev() {
//     res[i] *= postfix;
//     postfix *= nums[i];
// }

// res
