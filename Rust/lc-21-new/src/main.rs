pub struct Solution {}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    pub fn merge_two_lists(
        mut list1: Option<Box<ListNode>>,
        mut list2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut dummy = ListNode::new(0); // Create a dummy node
        let mut curr = &mut dummy; // Use a mutable reference to dummy

        while let (Some(l1), Some(l2)) = (list1.as_ref(), list2.as_ref()) {
            if l1.val < l2.val {
                curr.next = list1; // Attach list1 node to curr.next
                curr = curr.next.as_mut().unwrap(); // Move curr to the next node
                list1 = curr.next.take(); // Move list1 to the next node
            } else {
                curr.next = list2; // Attach list2 node to curr.next
                curr = curr.next.as_mut().unwrap(); // Move curr to the next node
                list2 = curr.next.take(); // Move list2 to the next node
            }
        }

        // Attach the remaining nodes of list1 or list2
        curr.next = if list1.is_some() { list1 } else { list2 };

        dummy.next // Return the merged list, skipping the dummy node

        // let mut curr = ListNode::new(0);
        // let head = &mut curr;

        // while let (Some( l1), Some( l2)) = (list1.as_ref(), list2.as_ref()) {
        //     if l1.val < l2.val {
        //         let node = ListNode::new(l1.val);
        //         curr.next = Some(Box::new(node));
        //         curr = *curr.next.unwrap();
        //         list1 = l1.next.clone();
        //     } else {
        //         let node = ListNode::new(l2.val);
        //         curr.next = Some(Box::new(node));
        //         curr = curr.next.unwrap();

        //         list2 = l2.next.clone();
        //     }
        // }

        // while let Some(l1) = list1.as_ref() {
        //     let node = ListNode::new(l1.val);
        //     curr.next = Some(Box::new(node));
        //     curr = *curr.next.unwrap();

        //     list1 = l1.next.clone();
        // }
        // while let Some(l2) = list2.as_ref() {
        //     let node = ListNode::new(l2.val);
        //     curr.next = Some(Box::new(node));
        //     curr = *curr.next.unwrap();

        //     list2 = l2.next.clone();
        // }

        // head.next
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let list1 = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 2,
                next: Some(Box::new(ListNode { val: 4, next: None })),
            })),
        }));
        let list2 = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 3,
                next: Some(Box::new(ListNode { val: 4, next: None })),
            })),
        }));
        let output = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode {
                    val: 2,
                    next: Some(Box::new(ListNode {
                        val: 3,
                        next: Some(Box::new(ListNode {
                            val: 4,
                            next: Some(Box::new(ListNode { val: 4, next: None })),
                        })),
                    })),
                })),
            })),
        }));
        assert_eq!(output, Solution::merge_two_lists(list1, list2));
    }
    #[test]
    fn test_2() {
        let list1 = None;
        let list2 = None;
        let list3 = None;

        assert_eq!(list3, Solution::merge_two_lists(list1, list2));
    }
    #[test]
    fn test_3() {
        let list1 = None;
        let list2 = Some(Box::new(ListNode { val: 0, next: None }));
        let output = Some(Box::new(ListNode { val: 0, next: None }));
        assert_eq!(output, Solution::merge_two_lists(list1, list2));
    }
}

fn main() {
    let list1 = Some(Box::new(ListNode {
        val: 1,
        next: Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode { val: 4, next: None })),
        })),
    }));
    let list2 = Some(Box::new(ListNode {
        val: 1,
        next: Some(Box::new(ListNode {
            val: 3,
            next: Some(Box::new(ListNode { val: 4, next: None })),
        })),
    }));
    println!("{:?}", Solution::merge_two_lists(list1, list2))
    // for i in 0..=30 {
    //     // println!("i: {}, val: {}",i,Solution::climb_stairs(i));
    //     println!(" {} => {},", i, Solution::fib(i));
    // }
}
