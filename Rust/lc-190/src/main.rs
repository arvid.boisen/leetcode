pub struct Solution {}

impl Solution {
    pub fn reverse_bits(x: u32) -> u32 {
        let (mut res, mut x) = (0u32, x);
        for _ in 0..32 {
            res = (res << 1) | (x & 1);
            x >>= 1;
            // println!("res: {:#b}", res);
            // println!("x: {:#b}", x);
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::reverse_bits(0b00000010100101000001111010011100),
            964176192
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            Solution::reverse_bits(0b11111111111111111111111111111101),
            3221225471
        );
    }
}

fn main() {}
