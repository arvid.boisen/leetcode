use std::collections::HashSet;

pub struct Solution {}

impl Solution {
    pub fn check_if_pangram(sentence: String) -> bool {
        let mut hashset = HashSet::new();
        for char in sentence.chars() {
            hashset.insert(char);
        }
            hashset.len() >= 26 
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            true,
            Solution::check_if_pangram(String::from("thequickbrownfoxjumpsoverthelazydog"))
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(false, Solution::check_if_pangram(String::from("leetcode")));
    }
}

fn main() {}
