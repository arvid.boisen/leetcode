use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut hashmap = HashMap::new();
        for (i, num) in nums.iter().enumerate() {
            let i = i as i32;
            let complement = target - num;
            if let Some(res) = hashmap.get(&complement) {
                return vec![*res, i];
            }
            hashmap.insert(num, i);
        }
        return vec![];
    }
}

/*

*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::two_sum(vec![2, 7, 1, 15], 9), vec![0, 1]);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::two_sum(vec![3, 2, 4], 6), vec![1, 2]);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::two_sum(vec![3, 3], 6), vec![0, 1]);
    }
}

fn main() {}
