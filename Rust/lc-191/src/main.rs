pub struct Solution {}

impl Solution {
    pub fn hamming_weight(mut n: i32) -> i32 {
        let mut counter = 0;
        while n != 0 {
            if n & 1 == 1 {
                counter += 1;
            }
            n >>= 1;
        }
        counter
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let result = Solution::hamming_weight(11);

        assert_eq!(result, 3);
    }
    #[test]
    fn test_2() {
        let result = Solution::hamming_weight(128);

        assert_eq!(result, 1);
    }
    #[test]
    fn test_3() {
        let result = Solution::hamming_weight(2147483645);

        assert_eq!(result, 30);
    }
}

fn main() {}
