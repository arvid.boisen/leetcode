pub struct Solution {}

impl Solution {
    pub fn fib(n: i32) -> i32 {
        let n = n as usize;
        let mut res = vec![0, 1];

        for i in 2..=n {
            res.push(res[i - 1] + res[i - 2]);
        }

        res[n]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(1, Solution::fib(2));
    }
    #[test]
    fn test_2() {
        assert_eq!(2, Solution::fib(3));

        // assert_eq!(3, Solution::climb_stairs(3));
    }
    #[test]
    fn test_3() {
        assert_eq!(3, Solution::fib(4));

        // assert_eq!(10946, Solution::climb_stairs(20));
    }
}

fn main() {
    for i in 0..=30 {
        // println!("i: {}, val: {}",i,Solution::climb_stairs(i));
        println!(" {} => {},", i, Solution::fib(i));
    }
}
