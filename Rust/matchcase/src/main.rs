fn main() {
    let k = 175;
    println!("Hello, world!");
    //println!("{}", square(30));
    //println!("{}", fib(k));
    println!("{}", fibacc((k,0,1)));
    
}

fn square(x: u128) -> u128 {
    match x {
        0 => 1,
        _ => x * square(x - 1),
    }
}


fn fib (x: u64) -> u64 {
    match x {
        0 => 0,
        1 => 1,
        _ => fib (x-1) + fib (x-2),
    }
}

fn fibacc ((x,y,z): (u32,u128,u128)) -> u128{
    match x {
        0 => y,
        _ => fibacc((x-1,y+z,y)),
    }

}

// fn fib((value, _): (i32, i32)) -> i32 { value }

// 0+1,1+1,1+2,2+3,3+5