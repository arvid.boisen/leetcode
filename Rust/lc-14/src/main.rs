pub struct Solution {}

impl Solution {
    pub fn longest_common_prefix(strs: Vec<String>) -> String {
        let mut output = strs[0].clone();
        for str in strs.iter() {
            while !str.starts_with(&output) {
                output.pop();
            }
        }
        return output;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::longest_common_prefix(vec![
                String::from("flower"),
                String::from("flow"),
                String::from("flight")
            ]),
            String::from("fl")
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            Solution::longest_common_prefix(vec![
                String::from("dog"),
                String::from("racecar"),
                String::from("car")
            ]),
            String::from("")
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            Solution::longest_common_prefix(vec![String::from("a")]),
            String::from("a")
        );
    }
}

fn main() {}
