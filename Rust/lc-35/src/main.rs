use std::cmp::Ordering;

pub struct Solution {}

impl Solution {
    pub fn search_insert(nums: Vec<i32>, target: i32) -> i32 {
        let mut low = 0;
        let mut high = (nums.len() - 1) as i32;

        while high >= low {
            let mid = (high + low) / 2;

            match target.cmp(&nums[mid as usize]) {
                Ordering::Greater => {
                    low = mid + 1;
                }
                Ordering::Less => {
                    high = mid - 1;
                }
                Ordering::Equal => {
                    return mid as i32;
                }
            }
        }
        low
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 5), 2);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 2), 1);
    }
    #[test]
    fn test_3() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 7), 4);
    }
    #[test]
    fn test_4() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 0), 0);
    }
    #[test]
    fn test_5() {
        assert_eq!(Solution::search_insert(vec![1, 3], 2), 1);
    }
}

fn main() {}
