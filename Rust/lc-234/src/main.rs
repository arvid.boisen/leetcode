use std::mem::replace;

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

pub struct Solution {}

impl Solution {
    pub fn is_palindrome(head: Option<Box<ListNode>>) -> bool {
        head == Solution::reverse_list(&head)
    }
    fn reverse_list(head: &Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let (mut prev, mut head)=(None, head.clone());
        while let Some(mut node) = head {
            head = replace(&mut node.next, prev);
            prev = Some(node);
        }
        prev
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::is_palindrome(Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode {
                    val: 2,
                    next: Some(Box::new(ListNode {
                        val: 2,
                        next: Some(Box::new(ListNode { val: 1, next: None })),
                    })),
                })),
            }))),
            true
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            Solution::is_palindrome(Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode { val: 2, next: None }))
            }))),
            false
        );
    }
}

fn main() {}
