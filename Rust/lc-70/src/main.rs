pub struct Solution {}

impl Solution {
    pub fn climb_stairs(n: i32) -> i32 {
        let n = n as usize;
        let mut res = vec![1, 2];
        for i in 2..n {
            res.push(res[i - 1] + res[i - 2]);
        }

        res[n - 1]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(2, Solution::climb_stairs(2));
    }
    #[test]
    fn test_2() {
        assert_eq!(3, Solution::climb_stairs(3));
    }
    #[test]
    fn test_3() {
        assert_eq!(10946, Solution::climb_stairs(20));
    }
    #[test]
    fn test_4() {
        assert_eq!(1836311903, Solution::climb_stairs(45));
    }
}

fn main() {
    for i in 1..=45 {
        // println!("i: {}, val: {}",i,Solution::climb_stairs(i));
        println!(" {} => {},", i, Solution::climb_stairs(i));
    }
}
