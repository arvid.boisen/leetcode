pub struct Solution {}

impl Solution {
    pub fn count_bits(n: i32) -> Vec<i32> {
        let n = (n + 1) as usize;
        let mut count = vec![0; n];
        for i in 1..n {
            count[i] = count[i & (i - 1)] + 1;
        }
        count
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::count_bits(2), vec![0, 1, 1]);
    }
    #[test]
    fn test_2() {
        assert_eq!(Solution::count_bits(5), vec![0, 1, 1, 2, 1, 2]);
    }
}

fn main() {}
