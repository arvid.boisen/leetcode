pub struct Solution {}

impl Solution {
    pub fn max_width_of_vertical_area(mut points: Vec<Vec<i32>>) -> i32 {
        points.sort_unstable_by_key(|v| v[0]);
        points.windows(2).map(|v| v[1][0] - v[0][0]).max().unwrap()
    }
    pub fn max_width_of_vertical_area1(points: Vec<Vec<i32>>) -> i32 {
        let mut area = 0;
        for curr in 0..points.len() {
            let mut res: Option<i32> = None;
            for point in 0..points.len() {
                let x = points[point][0];
                let curr_x = points[curr][0];
                if x>curr_x {
                    if let Some(val) = res{
                        res = Some(val.min(x-curr_x));

                    } else {
                        res=Some(x-curr_x);
                    }
                }

            }
            if let Some(val) = res {
                area = area.max(val);
            }
        }
        area
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            1,
            Solution::max_width_of_vertical_area(vec![
                vec![8, 7],
                vec![9, 9],
                vec![7, 4],
                vec![9, 7]
            ])
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            3,
            Solution::max_width_of_vertical_area(vec![
                vec![3, 1],
                vec![9, 0],
                vec![1, 0],
                vec![1, 4],
                vec![5, 3],
                vec![8, 8]
            ])
        );
    }
}

fn main() {}
