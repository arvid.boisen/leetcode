// use std::collections:: HashSet;

// use std::ops::{BitAnd, Shl, Shr, ShrAssign};

pub struct Solution {}

impl Solution {
    pub fn single_number(nums: Vec<i32>) -> Vec<i32> {
        let mut common = nums.iter().fold(0, |acc, x| acc ^ x);

        let mut position = 0;
        for i in 0..32 {
            if common & 1 == 1 {
                position = i;
                break;
            } else {
                common >>= 1;
            }
        }

        let group_a = nums
            .iter()
            .filter(|x| (*x & (1 << position)) != 0)
            .fold(0, |acc, x| acc ^ x);

        let group_b = nums
            .iter()
            .filter(|x| *x & (1 << (position)) == 0)
            .fold(0, |acc, x| acc ^ x);

        vec![group_a, group_b]
    }
}

// common.shr_assign(1);
// let group_a = nums.iter().filter(|x| x.bitand(1.shl(position))!=1).fold(0, |acc, x| acc ^ x);
// let group_b = nums.iter().filter(|x| x.bitand(1.shl(position))==0).fold(0, |acc, x| acc ^ x);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let mut result = Solution::single_number(vec![1, 2, 1, 3, 2, 5]);
        result.sort();
        assert_eq!(result, vec![3, 5]);
    }
    #[test]
    fn test_2() {
        let mut result = Solution::single_number(vec![-1, 0]);
        result.sort();
        assert_eq!(result, vec![-1, 0]);
    }
    #[test]
    fn test_3() {
        let mut result = Solution::single_number(vec![0, 1]);
        result.sort();
        assert_eq!(result, vec![0, 1]);
    }
}

fn main() {}
