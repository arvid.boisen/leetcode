use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut hashmap: HashMap<i32, i32> = HashMap::new();
        for (i, num) in nums.iter().enumerate() {
            let complement = target - num;
            if let Some(pos) = hashmap.get(&complement) {
                return vec![*pos, i.try_into().unwrap()];
            }
            hashmap.insert(*num, i.try_into().unwrap());
        }
        vec![]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(vec![0, 1], Solution::two_sum(vec![2, 7, 11, 15], 9));
    }
    #[test]
    fn test_2() {
        assert_eq!(vec![1, 2], Solution::two_sum(vec![3, 2, 4], 6));
    }
    #[test]
    fn test_3() {
        assert_eq!(vec![0, 1], Solution::two_sum(vec![3, 3], 6));
    }
}

fn main() {}

/*

        let mut hashmap: HashMap<i32, i32> = HashMap::new();

        for (pos, num) in nums.iter().enumerate() {
            let complement = target - num;
            if let Some(val) = hashmap.get(&complement){
                return vec![*val,pos.try_into().unwrap()];
            }
            hashmap.insert(*num, pos.try_into().unwrap());
        }

*/
// for (i, val) in nums.iter().enumerate() {
//     hashmap.insert(*val, i.try_into().unwrap());
// }

// for (i, val) in nums.iter().enumerate() {
//     let complement = target - val;
//     if hashmap.contains_key(&complement)
//         && hashmap.get(&complement) != Some(&i.try_into().unwrap())
//     {
//         return vec![i.try_into().unwrap(), *hashmap.get(&complement).unwrap()];
//     }
// }
