pub struct Solution {}

impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack = Vec::new();
        for i in s.chars() {
            match i {
                '{' => stack.push('}'),
                '(' => stack.push(')'),
                '[' => stack.push(']'),
                '}'|')'|']' if Some(i) != stack.pop() => return false,
                _ => (),
            }
        }
        return stack.is_empty();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert!(Solution::is_valid(String::from("()")));
    }
    #[test]
    fn test_2() {
        assert!(Solution::is_valid(String::from("()[]{}")));
    }
    #[test]
    fn test_3() {
        assert!(!Solution::is_valid(String::from("(]")));
    }
    #[test]
    fn test_4() {
        assert!(Solution::is_valid(String::from("({[()]})")));
    }
}

fn main() {}
