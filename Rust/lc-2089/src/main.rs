pub struct Solution {}

impl Solution {
    pub fn target_indices(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut res: Vec<i32> = Vec::new();
        let mut count =0;
        let mut lessthan=0;
        for num in nums{
            if num==target {
                count+=1;
            }
            if num<target {
                lessthan+=1;
            }
        }
        for i in 0..count{
            res.push(lessthan+i);
        }
        res

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            vec![1,2],
            Solution::target_indices(vec![1,2,5,2,3], 2)
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            vec![3],
            Solution::target_indices(vec![1,2,5,2,3], 3)
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            vec![4],
            Solution::target_indices(vec![1,2,5,2,3], 5)
        );
    }
}

fn main() {}
