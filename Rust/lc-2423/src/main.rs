use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    pub fn equal_frequency(word: String) -> bool {
                let  str: Vec<char> = word[..].chars().collect();
                    
                    
                    // str.sort();
                    // str.freq
                    let freq = str.iter()
                    .copied()
                    .fold(HashMap::new(), |mut map, val|{
                        map.entry(val)
                           .and_modify(|frq|*frq+=1)
                           .or_insert(1);
                        map
                    });
                    let mut count =0;
                    for char in word.chars() {
                        if let Some(x) = freq.get(&char){
                            if count>=*x-1 && count<=*x+1 {
                                
                            }
                        }
                    }
        
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(true, Solution::equal_frequency(String::from("abcc")));
    }
    #[test]
    fn test_2() {
        assert_eq!(false, Solution::equal_frequency(String::from("aazz")));
    }
}

fn main() {}
