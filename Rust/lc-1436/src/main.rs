use std::collections::HashSet;

pub struct Solution {}

impl Solution {
    pub fn dest_city(paths: Vec<Vec<String>>) -> String {
        for link in paths.iter(){
            let outgoing = &link[1];
            let mut found = false;
            for dest in paths.iter() {
                let incoming = &dest[0];
                if incoming==outgoing {
                    found=true;
                    break;
                }
            }
            if !found {
                return outgoing.to_string();
            }
        }
        String::new()
    }
    pub fn dest_city1(mut paths: Vec<Vec<String>>) -> String {
        let set: HashSet<String> = paths.iter_mut().map(|v| v.remove(0)).collect();
        
        for mut v in paths {
            if !set.contains(&v[0]) {
                return v.remove(0);
            }
        } 
        
        String::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = vec![
            vec!["London".to_owned(), "New York".to_owned()],
            vec!["New York".to_owned(), "Lima".to_owned()],
            vec!["Lima".to_owned(), "Sao Paulo".to_owned()],
        ];
        let output = Solution::dest_city1(input);

        assert_eq!("Sao Paulo".to_owned(), output);
    }
    #[test]
    fn test_2() {
        let input = vec![
            vec!["B".to_owned(), "C".to_owned()],
            vec!["D".to_owned(), "B".to_owned()],
            vec!["C".to_owned(), "A".to_owned()],
        ];
        let output = Solution::dest_city1(input);

        assert_eq!("A".to_owned(), output);
    }
    #[test]
    fn test_3() {
        let input = vec![vec!["A".to_owned(), "Z".to_owned()]];
        let output = Solution::dest_city1(input);

        assert_eq!("Z".to_owned(), output);
    }
}

fn main() {}
