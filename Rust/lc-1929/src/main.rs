pub struct Solution {}

impl Solution {
    pub fn get_concatenation(mut nums: Vec<i32>) -> Vec<i32> {
        nums.extend(nums.clone());
        nums
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            vec![1, 2, 1, 1, 2, 1],
            Solution::get_concatenation(vec![1, 2, 1])
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            vec![1, 3, 2, 1, 1, 3, 2, 1],
            Solution::get_concatenation(vec![1, 3, 2, 1])
        );
    }
}

fn main() {}
