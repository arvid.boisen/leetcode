pub struct Solution {}

impl Solution {
    pub fn count_consistent_strings(allowed: String, words: Vec<String>) -> i32 {
        let mut count = 0;
        for word in words {
            let mut found = true;
            for char in word.chars() {
                if !allowed.contains(char) {
                    found = false;
                    break;
                }
            }
            if found {
                count += 1;
            }
        }
        count
    }
    pub fn count_consistent_strings1(allowed: String, words: Vec<String>) -> i32 {
        let mut ch = 0;
        let mut ans = words.len();
        for c in allowed.chars() {
            ch |= 1 << (c as u8 - ('a' as u8));
        }
        for word in words {
            for c in word.chars() {
                if (1 & (ch >> (c as u8 - ('a' as u8)))) == 0 {
                    ans -= 1;
                    break;
                }
            }
        }
        ans as i32
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            2,
            Solution::count_consistent_strings(
                String::from("ab"),
                vec![
                    String::from("ad"),
                    String::from("bd"),
                    String::from("aaab"),
                    String::from("baa"),
                    String::from("badab")
                ]
            )
        );
    }
    #[test]
    fn test_2() {
        assert_eq!(
            7,
            Solution::count_consistent_strings(
                String::from("abc"),
                vec![
                    String::from("a"),
                    String::from("b"),
                    String::from("c"),
                    String::from("ab"),
                    String::from("ac"),
                    String::from("bc"),
                    String::from("abc")
                ]
            )
        );
    }
    #[test]
    fn test_3() {
        assert_eq!(
            4,
            Solution::count_consistent_strings(
                String::from("cad"),
                vec![
                    String::from("cc"),
                    String::from("acd"),
                    String::from("b"),
                    String::from("ba"),
                    String::from("bac"),
                    String::from("bad"),
                    String::from("ac"),
                    String::from("d")
                ]
            )
        );
    }

    #[test]
    fn test_4() {
        assert_eq!(
            2,
            Solution::count_consistent_strings1(
                String::from("ab"),
                vec![
                    String::from("ad"),
                    String::from("bd"),
                    String::from("aaab"),
                    String::from("baa"),
                    String::from("badab")
                ]
            )
        );
    }
    #[test]
    fn test_5() {
        assert_eq!(
            7,
            Solution::count_consistent_strings1(
                String::from("abc"),
                vec![
                    String::from("a"),
                    String::from("b"),
                    String::from("c"),
                    String::from("ab"),
                    String::from("ac"),
                    String::from("bc"),
                    String::from("abc")
                ]
            )
        );
    }
    #[test]
    fn test_6() {
        assert_eq!(
            4,
            Solution::count_consistent_strings1(
                String::from("cad"),
                vec![
                    String::from("cc"),
                    String::from("acd"),
                    String::from("b"),
                    String::from("ba"),
                    String::from("bac"),
                    String::from("bad"),
                    String::from("ac"),
                    String::from("d")
                ]
            )
        );
    }
}

fn main() {}
