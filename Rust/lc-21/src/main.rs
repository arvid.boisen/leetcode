pub struct Solution {}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

impl Solution {
    pub fn merge_two_lists(
        list1: Option<Box<ListNode>>,
        list2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        match (list1, list2) {
            (Some(node1), Some(node2)) => {
                if node1.val < node2.val {
                    Some(Box::new(ListNode {
                        val: node1.val,
                        next: Solution::merge_two_lists(node1.next, Some(node2)),
                    }))
                } else {
                    Some(Box::new(ListNode {
                        val: node2.val,
                        next: Solution::merge_two_lists(Some(node1), node2.next),
                    }))
                }
            }
            (Some(node), None) | (None, Some(node)) => Some(node),

            (None, None) => None,
        }

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let list1 = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 2,
                next: Some(Box::new(ListNode { val: 4, next: None })),
            })),
        }));
        let list2 = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 3,
                next: Some(Box::new(ListNode { val: 4, next: None })),
            })),
        }));
        let output = Some(Box::new(ListNode {
            val: 1,
            next: Some(Box::new(ListNode {
                val: 1,
                next: Some(Box::new(ListNode {
                    val: 2,
                    next: Some(Box::new(ListNode {
                        val: 3,
                        next: Some(Box::new(ListNode {
                            val: 4,
                            next: Some(Box::new(ListNode { val: 4, next: None })),
                        })),
                    })),
                })),
            })),
        }));
        assert_eq!(output, Solution::merge_two_lists(list1, list2));
    }
    #[test]
    fn test_2() {
        let list1 = None;
        let list2 = None;
        let list3 = None;

        assert_eq!(list3, Solution::merge_two_lists(list1, list2));
    }
    #[test]
    fn test_3() {
        let list1 = None;
        let list2 = Some(Box::new(ListNode { val: 0, next: None }));
        let output = Some(Box::new(ListNode { val: 0, next: None }));
        assert_eq!(output, Solution::merge_two_lists(list1, list2));
    }
}

fn main() {
    let list1 = Some(Box::new(ListNode {
        val: 1,
        next: Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode { val: 4, next: None })),
        })),
    }));
    let list2 = Some(Box::new(ListNode {
        val: 1,
        next: Some(Box::new(ListNode {
            val: 3,
            next: Some(Box::new(ListNode { val: 4, next: None })),
        })),
    }));
    println!("{:?}", Solution::merge_two_lists(list1, list2))
    // for i in 0..=30 {
    //     // println!("i: {}, val: {}",i,Solution::climb_stairs(i));
    //     println!(" {} => {},", i, Solution::fib(i));
    // }
}

/*
        if let Some(node1) = list1 {
            if let Some(node2) = list2 {
                if node1.val < node2.val {
                    return Some(Box::new(ListNode {
                        val: node1.val,
                        next: Solution::merge_two_lists(node1.next, Some(node2))
                    }));
                } else {
                    return Some(Box::new(ListNode {
                        val: node2.val,
                        next: Solution::merge_two_lists(Some(node1), node2.next)
                    }));
                }
            } else {
                return Some(Box::new(ListNode {
                    val: node1.val,
                    next: Solution::merge_two_lists(node1.next, None),
                }));
            }
        }
        if let Some(node2) = list2 {
            return Some(Box::new(ListNode {
                val: node2.val,
                next: Solution::merge_two_lists(None, node2.next),
            }));
        }

        None
*/

// loop {
//     if let Some(one) = l1 {
//         let listnode;
//         if let Some(two) = l2 {
//             if one.val > two.val {
//                 listnode = ListNode {
//                     val: one.val,
//                     next: Some(Box::new(ListNode::new(two.val))),
//                 };
//             } else {
//                 listnode = ListNode {
//                     val: two.val,
//                     next: Some(Box::new(ListNode::new(one.val))),
//                 };
//             };
//         } else {
//             listnode = ListNode::new(one.val);
//         };
//         let mut output = Some(Box::new(listnode));
//     }
//     l1 = l1.unwrap().next;
// }

/*

        let mut iter1 = list1.as_ref();
        let mut iter2 = list2.as_ref();
        let mut cur_node: Option<Box<ListNode>> = None;

        while iter1.is_some() && iter2.is_some() {
            if let Some(node1) = iter1 {
                if let Some(node2) = iter2 {
                    let mut new_node;
                    if node1.val > node2.val {
                        new_node = ListNode {
                            val: node1.val,
                            next: Some(Box::new(ListNode {
                                val: node2.val,
                                next: None,
                            })),
                        };
                    } else {
                        new_node = ListNode {
                            val: node2.val,
                            next: Some(Box::new(ListNode {
                                val: node1.val,
                                next: None,
                            })),
                        };
                    }
                    if cur_node.is_some() {
                        if let Some(cur_box) = cur_node {
                            new_node.next = Some(cur_box);
                        }
                    }
                    cur_node = Some(Box::new(new_node));
                    iter1 = node1.next.as_ref();
                    iter2 = node2.next.as_ref();
                }
            }
        }
        while let Some(ref_node) = cur_node.as_ref() {
            println!("{}",ref_node.val);
            cur_node = ref_node.next.clone();
        }


        cur_node
        // match Some(one) = l1 {

        // }

        // None

*/

/*

        let mut l1 = list1;
        let mut l2 = list2;
        let mut r = &mut l1;
        while l2.is_some() {
            if r.is_none() || l2.as_ref()?.val < r.as_ref()?.val {
                std::mem::swap(r, &mut l2);
            }
            r = &mut r.as_mut()?.next;
        }
        l1
*/
