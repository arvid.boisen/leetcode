use std::collections::LinkedList;

pub struct Solution {}

impl Solution {
    pub fn set_zeroes(matrix: &mut Vec<Vec<i32>>) {
        let mut stack = LinkedList::new();

        for x in 0..matrix.len() {
            for y in 0..matrix[x].len() {
                if matrix[x][y] == 0 {
                    stack.push_front((x, y));
                }
            }
        }
        while let Some((y, x)) = stack.pop_front() {
            for k in 0..matrix[y].len() {
                matrix[y][k] = 0;
            }
            for k in 0..matrix.len() {
                matrix[k][x] = 0;
            }
        }
    }
    pub fn set_zeroes1(matrix: &mut Vec<Vec<i32>>) {
        let mut col0 = true;
        let rows = matrix.len();
        let cols = matrix[0].len();

        for i in 0..rows {
            if matrix[i][0] == 0 {
                col0 = false;
                break;
            }
            for j in 1..cols {
                if matrix[i][j] == 0 {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        for i in (0..rows).rev() {
            for j in (0..cols).rev() {
                if (matrix[i][0] == 0) || matrix[0][j] == 0 {
                    matrix[i][j] = 0;
                }
            }
            if !col0 {
                matrix[i][0] = 0;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let mut input = vec![vec![1, 1, 1], vec![1, 0, 1], vec![1, 1, 1]];
        let output = vec![vec![1, 0, 1], vec![0, 0, 0], vec![1, 0, 1]];

        Solution::set_zeroes1(&mut input);
        assert_eq!(input, output);
    }
    #[test]
    fn test_2() {
        let mut input = vec![vec![0, 1, 2, 0], vec![3, 4, 5, 2], vec![1, 3, 1, 5]];
        let output = vec![vec![0, 0, 0, 0], vec![0, 4, 5, 0], vec![0, 3, 1, 0]];

        Solution::set_zeroes1(&mut input);
        assert_eq!(input, output);
    }
    #[test]
    fn test_3() {
        let mut input = vec![vec![-4,-2147483648,6,-7,0],vec![-8,6,-8,-6,0],vec![2147483647,2,-9,-6,-10]];
        let output = vec![vec![0,0,0,0,0],vec![0,0,0,0,0],vec![2147483647,2,-9,-6,0]];

        Solution::set_zeroes1(&mut input);
        assert_eq!(input, output);
    }
}

fn main() {}
