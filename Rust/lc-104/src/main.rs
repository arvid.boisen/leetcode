use std::cell::RefCell;
use std::rc::Rc;

pub struct Solution {}
#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}
impl Solution {
    pub fn max_depth(root: Option<Rc<RefCell<TreeNode>>>) -> i32 {
        if let Some(node) = root {
            let node = node.borrow();

            let max_left = Solution::max_depth(node.left.clone());
            let max_right = Solution::max_depth(node.right.clone());
            return i32::max(max_left, max_right) + 1;
        } else {
            return 0;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let root = Some(Rc::new(RefCell::new(TreeNode {
            val: 1,
            left: Some(Rc::new(RefCell::new(TreeNode::new(2)))),
            right: Some(Rc::new(RefCell::new(TreeNode {
                val: 3,
                left: Some(Rc::new(RefCell::new(TreeNode::new(4)))),
                right: Some(Rc::new(RefCell::new(TreeNode::new(5)))),
            }))),
        })));
        assert_eq!(3, Solution::max_depth(root));
    }

    #[test]
    fn test_max_depth_empty_tree() {
        let root = None;
        assert_eq!(Solution::max_depth(root), 0);
    }

    #[test]
    fn test_max_depth_single_node_tree() {
        let root = Some(Rc::new(RefCell::new(TreeNode::new(5))));
        assert_eq!(Solution::max_depth(root), 1);
    }
    // #[test]
    // fn test_2() {
    //     assert_eq!(
    //         2,
    //         Solution::max_depth(

    //         )
    //     );
    // }
}

fn main() {}
