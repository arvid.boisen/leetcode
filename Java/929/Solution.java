import java.util.HashSet;

class Solution {

    public int numUniqueEmails(String[] emails) {
        HashSet<String> set = new HashSet<>();
        for (String email:emails) {
            String[] parts = email.split("@");
            
            String[] local = parts[0].split("\\+");
            set.add(local[0].replace(".","")+"@"+parts[1]);



            // StringBuilder email = new StringBuilder(emails[i]);

            // int atIndex = email.indexOf("@");
            // while (email.indexOf(".") < atIndex && email.indexOf(".")>=0) {
            //     email.deleteCharAt(email.indexOf("."));
            //     atIndex = email.indexOf("@");
            // }
            // int plusIndex = email.indexOf("+");
            // atIndex = email.indexOf("@");
            // if (plusIndex >= 0 && plusIndex<atIndex)
            //     email.delete(plusIndex, atIndex);
            // System.out.println(email.toString());
            // set.add(email.toString());
        }

        return set.size();
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.numUniqueEmails(new String[] { "j+d.j.b.k.xr.mmp@rn.qyy.com","j+yt+w.on.k.r+i.l@rn.qyy.com","j+i.t.b.o.x.l.s.a.z@rn.qyy.com","j+wteuapmm.y@rn.qyy.com","q.z.y.znvz.d+l+p@kyf.com","np.e.x+u.a+mbv+j@uadsua.rqda.com","np.e.x+e.f.n.f.r.c@uadsua.rqda.com","pdiykt.rh+cc@ta.bxx.com","j+aqlxgyy+b.k@rn.qyy.com","j+a.hm.y.t.j.d+qq@rn.qyy.com","k.i.j.hy.pe.n+ea@xfeslns.com","j+h.v.w.b+x+h.e.n.r@rn.qyy.com","j+m+j.k.o.jl.vv+r@rn.qyy.com","k.i.j.hy.pe.n+l+i@xfeslns.com","k.i.j.hy.pe.n+nh@xfeslns.com","j+akd.xb.xx+c.e@rn.qyy.com","j+a.j.u+e.s.p+w.x.x@rn.qyy.com" }));
        System.out.println(solution.numUniqueEmails(new String[] { "test.email+alex@leetcode.com",
                "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com" }));
        System.out.println(
                solution.numUniqueEmails(new String[] { "a@leetcode.com", "b@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com", "c@leetcode.com" }));

    }
}

