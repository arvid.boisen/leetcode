class Solution {

    public int lengthOfLastWord(String s) {
        int last_index = s.length() - 1;
        int counter = 0;
        boolean initial_condition = true;

        while (s.charAt(last_index) != ' ' || initial_condition) {
            if (s.charAt(last_index) != ' ') {
                initial_condition = false;
                counter++;
                last_index--;
                if (last_index<0)
                    return counter;
            } else {
                last_index--;

            }
        }

        return counter;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.lengthOfLastWord("Hello World"));
        System.out.println(solution.lengthOfLastWord("   fly me   to   the moon  "));
        System.out.println(solution.lengthOfLastWord("luffy is still joyboy"));
        System.out.println(solution.lengthOfLastWord("luffy is still joyboy         "));
        System.out.println(solution.lengthOfLastWord("a"));

    }
}