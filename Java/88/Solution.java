import java.util.Arrays;

class Solution {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {

        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;

        while (j >= 0) {
            if (i >= 0 && nums1[i] > nums2[j]) {
                nums1[k--] = nums1[i--];
            } else {
                nums1[k--] = nums2[j--];
            }
        }

    }

    public static void merge1(int[] nums1, int m, int[] nums2, int n) {
        for (int j = 0, i = m; j < n; j += 1) {
            nums1[i] = nums2[j];
            i++;
        }
        Arrays.sort(nums1);
    }

    public static void main(String[] args) {
        int[] nums1_1 = new int[] { 1, 2, 3, 0, 0, 0 };
        merge1(nums1_1, 3, new int[] { 2, 5, 6 }, 3);
        System.out.println(Arrays.toString(nums1_1));

        int[] nums1_2 = new int[] { 1 };
        merge1(nums1_2, 3, new int[] { 0 }, 0);
        System.out.println(Arrays.toString(nums1_2));

        int[] nums1_3 = new int[] { 0 };
        merge1(nums1_3, 0, new int[] { 1 }, 1);
        System.out.println(Arrays.toString(nums1_3));

        merge(nums1_1, 3, new int[] { 2, 5, 6 }, 3);
        System.out.println(Arrays.toString(nums1_1));

        merge(nums1_2, 3, new int[] { 0 }, 0);
        System.out.println(Arrays.toString(nums1_2));

        merge(nums1_3, 0, new int[] { 1 }, 1);
        System.out.println(Arrays.toString(nums1_3));

    }
}