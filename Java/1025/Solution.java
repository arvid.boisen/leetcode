class Solution {
    enum Player {
        ALICE, BOB
    }

    static public boolean divisorGame(int n) {
        Player player = Player.ALICE;

        while (n > 1)
            for (int x = n - 1; x > 0; x--) {
                if (n % x == 0) {
                    n -= x;
                    if (player.equals(Player.ALICE))
                        player = Player.BOB;
                    else
                        player = Player.ALICE;

                    continue;
                }
            }
        return player.equals(Player.BOB);

    }

    public static void main(String[] args) {
        System.out.println(divisorGame(2));
        System.out.println(divisorGame(3));
        System.out.println(divisorGame(5));

    }
}