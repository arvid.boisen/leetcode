import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FindTargetIndicesAfterSortingArray {
    public List<Integer> targetIndices1(int[] nums, int target) {
        LinkedList<Integer> res = new LinkedList<>();
        Arrays.sort(nums);
        int high = nums.length - 1;
        int low = 0;
        while (low <= high) {
            int median = (high + low) / 2;
            if (nums[median] < target)
                low = median + 1;
            else if (nums[median] > target)
                high = median - 1;
            else {
                res.add(median);
                int i = 1;
                while (nums.length > median + i && nums[median + i] == target)
                    res.addLast(median + i++);
                i = 1;
                while (median - i >= 0 && nums[median - i] == target)
                    res.addFirst(median - i++);
                break;
            }

        }
        return res;
    }

    public List<Integer> targetIndices(int[] nums, int target) {
        int count = 0, lessthan = 0;
        for (int n : nums) {
            if (n == target)
                count++;
            if (n < target)
                lessthan++;
        }

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < count; i++)
            result.add(lessthan++);

        return result;
    }

    void app() {
        int[] nums1 = { 1, 2, 5, 2, 3 };
        int target1 = 2;
        int[] nums2 = { 1, 2, 5, 2, 3 };
        int target2 = 3;
        int[] nums3 = { 1, 2, 5, 2, 3 };
        int target3 = 5;
        int[] nums4 = { 1 };
        int target4 = 2;
        System.out.println(targetIndices(nums1, target1));
        System.out.println(targetIndices(nums2, target2));
        System.out.println(targetIndices(nums3, target3));
        System.out.println(targetIndices(nums4, target4));

    }

    public static void main(String[] args) {
        FindTargetIndicesAfterSortingArray main = new FindTargetIndicesAfterSortingArray();
        main.app();
    }

}
