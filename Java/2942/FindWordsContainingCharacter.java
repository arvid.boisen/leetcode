import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class FindWordsContainingCharacter {
    static public List<Integer> findWordsContaining(String[] words, char x) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            if (words[i].indexOf(x) >= 0)
                res.add(i);
        }
        return res;
    }

    public static void main(String[] args) {
        String[] words1 = { "leet", "code" };
        char x1 = 'e';
        String[] words2 = { "abc", "bcd", "aaaa", "cbc" };
        char x2 = 'a';
        String[] words3 = { "abc", "bcd", "aaaa", "cbc" };
        char x3 = 'z';

        System.out.println(Arrays.toString(findWordsContaining(words1, x1).toArray()));
        System.out.println(Arrays.toString(findWordsContaining(words2, x2).toArray()));
        System.out.println(Arrays.toString(findWordsContaining(words3, x3).toArray()));
    }
}