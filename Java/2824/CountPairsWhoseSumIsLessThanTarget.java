import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CountPairsWhoseSumIsLessThanTarget {
    public int countPairs(List<Integer> nums, int target) {
        int count = 0;
        for (int i=0; i < nums.size(); i++)
            for (int j=i+1; j < nums.size(); j++ ){
                if (nums.get(i)+nums.get(j)<target)
                    count++;
            }

        return count;
    }
    void app(){
        List<Integer> nums1 = new ArrayList<Integer>(Arrays.asList(-1, 1, 2, 3, 1));
        int target1 = 2;
        List<Integer> nums2 = new ArrayList<Integer>(Arrays.asList(-6, 2, 5, -2, -7, -1, 3));
        int target2 = -2;

        System.out.println(countPairs(nums1, target1));
        System.out.println(countPairs(nums2, target2));
    }

    public static void main(String[] args) {
        CountPairsWhoseSumIsLessThanTarget main = new CountPairsWhoseSumIsLessThanTarget();
        main.app();


    }
}
