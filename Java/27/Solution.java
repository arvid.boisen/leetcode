class Solution {

    public int removeElement(int[] nums, int val) {
        char counter = 0;
        int last = nums.length - 1;
        for (char i = 0; i < nums.length && i <= last; i++) {
            if (nums[i] == val) {
                nums[i] = nums[last];
                nums[last] = -1;
                last--;
                i--;
            } else
                counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.removeElement(new int[] { 3, 2, 2, 3 }, 3));
        System.out.println(solution.removeElement(new int[] { 0, 1, 2, 2, 3, 0, 4, 2 }, 2));
        System.out.println(solution.removeElement(new int[] {}, 2));
        System.out.println(solution.removeElement(new int[] { 2, 2, 2, 2, 2, }, 2));
        System.out.println(solution.removeElement(new int[] { 2, 2, 2, 2, 2, }, 3));

    }
}
