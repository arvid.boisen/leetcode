import java.util.HashSet;

class Solution {

    static public boolean containsDuplicate(int[] nums) {
        HashSet<Integer> set = new HashSet<>();
        for (int i=0; i < nums.length; i++) {
            if (!set.add(nums[i]))
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(containsDuplicate(new int[] { 1,2,3,1}));
        System.out.println(containsDuplicate(new int[] { 1,2,3,4 }));
        System.out.println(containsDuplicate(new int[] {1,1,1,3,3,4,3,2,4,2 }));

    }
}